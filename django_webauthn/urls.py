from django.urls import path

from . import views


app_name = 'webauthn'
urlpatterns = [
    path('api/start-registration/', views.start_registration, name='start-registration'),
    path('api/register/', views.register, name='register'),
    path('api/start-auth/', views.start_authenticate, name='start-auth'),
    path('api/auth/', views.authenticate, name='auth'),
    path('api/creds/', views.get_creds, name='get-creds'),
    path('api/creds/<str:id>/', views.delete_cred, name='delete-creds'),
    path('api/new-device/', views.new_device_code, name='new-device-code'),
    path('api/new-device/start-add/', views.start_add_new_device, name='start-add-new-device'),
]
