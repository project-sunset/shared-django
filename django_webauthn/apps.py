from django.apps import AppConfig


class DjangoWebauthnConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django_webauthn'
