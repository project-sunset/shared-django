import json
import secrets
import string
from datetime import timedelta
from uuid import uuid4
from base64 import b64decode

from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.utils import timezone
from shared_django.django_webauthn.models import (NewDeviceCode, WebAuthnCred,
                                                  WebAuthnUser)
from webauthn import (generate_authentication_options,
                      generate_registration_options, options_to_json,
                      verify_authentication_response,
                      verify_registration_response)
from webauthn.helpers import base64url_to_bytes, bytes_to_base64url
from webauthn.helpers.structs import (AuthenticatorAttachment,
                                      AuthenticatorSelectionCriteria,
                                      PublicKeyCredentialDescriptor)


def username_already_exists(username):
    return User.objects.filter(username=username).exists()


def start_registration(request: HttpRequest):
    username = json.load(request)['username']
    if username == '' or username is None:
        return JsonResponse(status=400, data={'error': 'Username cannot be empty'})
    if username_already_exists(username):
        return JsonResponse(status=400, data={'error': 'Username already exists'})
    user_uuid = uuid4()
    options = generate_registration_options(
        rp_id=settings.WEBAUTHN['rp_id'],
        rp_name='Project Sunset',
        user_id=base64url_to_bytes(user_uuid),
        user_name=username,
        authenticator_selection=AuthenticatorSelectionCriteria(
            authenticator_attachment=AuthenticatorAttachment.PLATFORM,
        )
    )
    request.session['webauthn'] = {
        'uuid': str(user_uuid),
        'username': username,
        'challenge': bytes_to_base64url(options.challenge)
    }
    return HttpResponse(options_to_json(options))


def register(request: HttpRequest):
    webauthn_session = request.session['webauthn']
    credentials = verify_registration_response(
        credential=request.body.decode(),
        expected_challenge=base64url_to_bytes(webauthn_session['challenge']),
        expected_origin=settings.WEBAUTHN['origin'],
        expected_rp_id=settings.WEBAUTHN['rp_id']
    )
    user, _ = User.objects.get_or_create(username=webauthn_session['username'])
    web_authn_user, _ = WebAuthnUser.objects.get_or_create(user_uuid=webauthn_session['uuid'], user=user)
    web_authn_cred = WebAuthnCred(user=web_authn_user,
                                  credential_public_key=credentials.credential_public_key,
                                  credential_id=credentials.credential_id,
                                  current_sign_count=0)
    web_authn_cred.save()
    login(request, user, backend='django.contrib.auth.backends.ModelBackend')
    webauthn_session['used_device'] = web_authn_cred.id
    if 'code' in webauthn_session:
        new_device_code = NewDeviceCode.objects.get(user=user, code=webauthn_session['code'])
        new_device_code.delete()
    return HttpResponse(status=204)


def start_authenticate(request: HttpRequest):
    username = json.load(request)['username']
    if username == '' or username is None:
        return JsonResponse(status=400, data={'error': 'Username cannot be empty'})
    creds = WebAuthnCred.objects.filter(user__user__username=username)
    options = generate_authentication_options(
        rp_id=settings.WEBAUTHN['rp_id'],
        allow_credentials=[PublicKeyCredentialDescriptor(id=cred.credential_id) for cred in creds]
    )
    request.session['webauthn'] = {
        'username': username,
        'challenge': bytes_to_base64url(options.challenge)
    }
    return HttpResponse(options_to_json(options))


def authenticate(request: HttpRequest):
    webauthn_session = request.session['webauthn']
    credential = json.loads(request.body.decode())
    creds = WebAuthnCred.objects.filter(user__user__username=webauthn_session['username'])
    public_key = creds.get(credential_id=base64url_to_bytes(credential['rawId']))
    auth = verify_authentication_response(
        credential=credential,
        expected_challenge=base64url_to_bytes(webauthn_session['challenge']),
        expected_origin=settings.WEBAUTHN['origin'],
        expected_rp_id=settings.WEBAUTHN['rp_id'],
        credential_public_key=public_key.credential_public_key,
        credential_current_sign_count=public_key.current_sign_count
    )
    public_key.current_sign_count = auth.new_sign_count
    public_key.save()
    login(request, public_key.user.user, backend='django.contrib.auth.backends.ModelBackend')
    webauthn_session['used_device'] = public_key.id
    return HttpResponse(status=204)


@login_required
def get_creds(request: HttpRequest):
    if 'webauthn' in request.session and 'used_device' in request.session['webauthn']:
        current_session_id = request.session['webauthn']['used_device']
    else:
        current_session_id = None
    return JsonResponse({'creds': [{
        'created': cred.created,
        'id': cred.id,
        'current_session': current_session_id == cred.id
    } for cred in WebAuthnCred.objects.filter(user__user=request.user)]})


@login_required
def delete_cred(request: HttpRequest, id: str):
    if request.method == 'DELETE':
        WebAuthnCred.objects.filter(user__user=request.user, id=id).delete()
        return HttpResponse(status=204)
    else:
        return HttpResponse(status=405)


@login_required
def new_device_code(request: HttpRequest):
    NewDeviceCode.objects.filter(user=request.user, created__lt=timezone.now() - timedelta(hours=1)).delete()
    try:
        code = NewDeviceCode.objects.get(user=request.user)
    except NewDeviceCode.DoesNotExist:
        code = None
    if request.method == 'POST':
        if code is not None:
            return JsonResponse({'code': code.code})
        new_code = ''.join(secrets.choice(string.digits) for _ in range(6))
        NewDeviceCode.objects.create(user=request.user, code=new_code)
        return JsonResponse({'code': new_code})
    elif request.method == 'DELETE':
        if code is not None:
            code.delete()
        return HttpResponse(status=204)
    elif request.method == 'GET':
        if code is None:
            return JsonResponse({'code': None})
        return JsonResponse({'code': code.code})


def start_add_new_device(request: HttpRequest):
    data = json.load(request)
    try:
        new_device_code = NewDeviceCode.objects.get(user__username=data['username'], code=data['code'])
        if timezone.now() > new_device_code.created + timedelta(hours=1):
            new_device_code.delete()
            return HttpResponse(status=400)
        webauthn_user, _ = WebAuthnUser.objects.get_or_create(user=new_device_code.user, defaults={'user_uuid': uuid4()})
        user_uuid = str(webauthn_user.user_uuid)
        options = generate_registration_options(
            rp_id=settings.WEBAUTHN['rp_id'],
            rp_name='Project Sunset',
            user_id=user_uuid,
            user_name=data['username'],
            authenticator_selection=AuthenticatorSelectionCriteria(
                authenticator_attachment=AuthenticatorAttachment.PLATFORM,
            )
        )
        request.session['webauthn'] = {
            'uuid': user_uuid,
            'username': data['username'],
            'challenge': bytes_to_base64url(options.challenge),
            'code': data['code']
        }
        return HttpResponse(options_to_json(options))
    except NewDeviceCode.DoesNotExist:
        return HttpResponse(status=400)
