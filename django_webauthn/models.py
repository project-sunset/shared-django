from django.contrib.auth.models import User
from django.db import models


class WebAuthnUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='webauthn_user')
    user_uuid = models.UUIDField(unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user} - {self.created}'


class WebAuthnCred(models.Model):
    user = models.ForeignKey(WebAuthnUser, on_delete=models.CASCADE, related_name='creds')
    credential_public_key = models.BinaryField()
    credential_id = models.BinaryField()
    current_sign_count = models.IntegerField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user} - {self.created}'


class NewDeviceCode(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=6)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.user} - {self.code}'
