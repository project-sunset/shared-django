from django.contrib import admin
from shared_django.django_webauthn.models import WebAuthnCred, WebAuthnUser, NewDeviceCode


@admin.register(WebAuthnUser)
class WebAuthnUserAdmin(admin.ModelAdmin):
    list_display = ('user',)


@admin.register(WebAuthnCred)
class WebAuthnCredAdmin(admin.ModelAdmin):
    list_display = ('user', 'created', 'current_sign_count')


@admin.register(NewDeviceCode)
class NewDeviceCodeAdmin(admin.ModelAdmin):
    list_display = ('user', 'created')
