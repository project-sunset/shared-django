from django.contrib.auth.models import User
from django.test import Client, TestCase, tag


@tag('webauthn')
class WebAuthnTests(TestCase):
    def test_new_device_code(self):
        User.objects.create_user('flubber', None, 'flubber')
        c = Client()
        c.login(username='flubber', password='flubber')

        response = c.get('/webauthn/api/new-device/')
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.json()['code'])

        response = c.post('/webauthn/api/new-device/')
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.json()['code'])

        response = c.get('/webauthn/api/new-device/')
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.json()['code'])

        response = c.delete('/webauthn/api/new-device/')
        self.assertEqual(response.status_code, 204)

        response = c.get('/webauthn/api/new-device/')
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(response.json()['code'])
