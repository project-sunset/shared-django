import json
import os
from shutil import rmtree
from subprocess import PIPE, STDOUT, Popen, run
from time import time

from django.core.management import call_command
from django.test import LiveServerTestCase, override_settings

PLAYWRIGHT_DIR = '../tests'

class VueException(Exception):
    pass

@override_settings(DEBUG=True)
class PlaywrightTests(LiveServerTestCase):
    js_coverage = []
    config = {}

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # Manage nyc
        rmtree('../.nyc_output', ignore_errors=True)
        os.mkdir('../.nyc_output')
        # instrument backend javascript
        for folder in cls.js_coverage:
            run(f'npx --yes nyc instrument ./{folder} --in-place', shell=True)
        call_command('collectstatic', '--noinput')
        for folder in cls.js_coverage:
            run(f'git checkout {folder}', shell=True)

        # run Vue server
        env = os.environ.copy()
        env["VITE_APP_API_URL"] = cls.live_server_url + '/'
        env["DISABLE_HMR"] = "true"
        cls.vue_server = Popen('npm run dev', cwd='../frontend/', env=env, shell=True, stdout=PIPE, stderr=STDOUT)
        start = time()
        output = []
        while True:
            line = cls.vue_server.stdout.readline()
            output.append(line.decode('utf-8'))
            if b'ready in ' in line:
                break
            if b'Failed to compile' in line:
                raise VueException('Failed to compile frontend')
            if cls.vue_server.poll() is not None:
                raise VueException(f"npm script failed:\n{''.join(output)}")
            if time() - start > 10:
                raise VueException(f"Timeout building Vue: {''.join(output)}\n")
        
        # provide info for Playwright to import
        cls.config['server_url'] = cls.live_server_url
        with open('../tests/config.json', 'w') as f:
            f.write(json.dumps(cls.config))

    @classmethod
    def tearDownClass(cls):
        cls.vue_server.terminate()
        cls.vue_server.wait()
        os.remove('../tests/config.json')
        super().tearDownClass()
    
    def run_playwright(self, playwright_project):
        result = run(f'npm run test-no-webserver -- --project {playwright_project}',
                     cwd=PLAYWRIGHT_DIR, shell=True, check=False)
        self.assertEqual(result.returncode, 0)