from django.urls import path
from shared_django.accounts.views import (change_email, change_password,
                                          change_username, delete_account,
                                          get_account_info, google_login,
                                          login_as, ps_login, ps_logout,
                                          unlink_google, check_user)

app_name = 'accounts'
urlpatterns = [
    path('login/', ps_login, name="login"),
    path('logout/', ps_logout, name="logout"),
    path('google-login/', google_login, name="google_login"),
    path('get-account/', get_account_info, name='get_account_info'),
    path('changepassword/', change_password, name="change_password"),
    path('changeemail/', change_email, name="change_email"),
    path('changeusername/', change_username, name="change_username"),
    path('unlinkgoogle/', unlink_google, name="unlink_google"),
    path('deleteaccount/', delete_account, name="delete_account"),
    path('loginas/<int:user_id>/', login_as, name="login_as"),
    path('check-user/', check_user, name='check_user')
]
