from django.db import models
from django.contrib.auth.models import User


class GoogleUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    google_id = models.CharField(max_length=100, null=True, blank=True, unique=True)


class ApiToken(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    token = models.UUIDField()
    expiry = models.DateTimeField()
