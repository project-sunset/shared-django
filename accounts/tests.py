import json

from django.test import Client, TestCase


class TestViews(TestCase):
    fixtures = ['user_test_data.json']

    def test_login(self):
        c = Client()
        response = c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "supergirl01"}), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["authenticated"], True)
        response = c.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["authenticated"], True)

    def test_failed_login(self):
        c = Client()
        response = c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "top_supergirl01"}), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["authenticated"], False)
        response = c.get("/accounts/login/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["authenticated"], False)

    def test_logout(self):
        c = Client()
        c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "supergirl01"}), content_type="application/json")
        response = c.get("/accounts/logout/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["success"], True)

    def test_change_password(self):
        c = Client()
        response = c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "supergirl01"}), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        response = c.post("/accounts/changepassword/", json.dumps({"current_password": "supergirl01", "new_password_1": "top_supergirl01",
                                                                   "new_password_2": "top_supergirl01"}), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["success"], True)

    def test_change_password_wrong_current(self):
        c = Client()
        c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "supergirl01"}), content_type="application/json")
        response = c.post("/accounts/changepassword/", json.dumps({"current_password": "seret", "new_password_1": "top_supergirl01",
                          "new_password_2": "top_supergirl01"}), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["success"], False)

    def test_change_password_wrong_new(self):
        c = Client()
        c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "supergirl01"}), content_type="application/json")
        response = c.post("/accounts/changepassword/", json.dumps({"current_password": "supergirl01",
                          "new_password_1": "top_supergirl01", "new_password_2": "top_secrt"}), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["success"], False)

    def test_change_email(self):
        c = Client()
        r = c.post("/accounts/login/", json.dumps({"username": "Kara", "password": "supergirl01"}), content_type="application/json")
        r = r.json()
        self.assertEqual(r['user']['email'], '')
        r = c.post('/accounts/changeemail/', json.dumps({"new_email": "test@test.nl"}), content_type="application/json")
        self.assertEqual(r.status_code, 200)
        r = c.get("/accounts/login/")
        self.assertEqual(r.json()['user']["email"], 'test@test.nl')
