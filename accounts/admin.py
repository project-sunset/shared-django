from django.contrib import admin
from shared_django.accounts.models import GoogleUser, ApiToken


@admin.register(GoogleUser)
class GoogleUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'google_id')


@admin.register(ApiToken)
class ApiTokenAdmin(admin.ModelAdmin):
    list_display = ('user', 'expiry')
