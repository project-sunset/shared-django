from importlib import import_module
import json

from django.conf import settings
from django.contrib.auth import (authenticate, login, logout,
                                 update_session_auth_hash)
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.http import HttpRequest, JsonResponse
from django.shortcuts import redirect
from google.auth.transport import requests
from google.oauth2 import id_token
from shared_django.accounts.models import GoogleUser
from shared_django.django_webauthn.models import WebAuthnCred


USER_DATA = None
if hasattr(settings, 'SHARED_DJANGO_USER_DATA'):
    USER_DATA = {
        'model': getattr(import_module(settings.SHARED_DJANGO_USER_DATA['model'][0]), settings.SHARED_DJANGO_USER_DATA['model'][1]),
        'serializer': getattr(import_module(settings.SHARED_DJANGO_USER_DATA['serializer'][0]), settings.SHARED_DJANGO_USER_DATA['serializer'][1]),
        'field_name': settings.SHARED_DJANGO_USER_DATA['field_name']
    }


def get_user_data(request, user):
    user_data = {
        'username': user.username,
        'superuser': user.is_superuser,
        'email': user.email,
    }
    if USER_DATA is not None:
        try:
            more_data = USER_DATA['model'].objects.get(user=user)
            user_data[USER_DATA['field_name']] = USER_DATA['serializer'](more_data).data
        except USER_DATA['model'].DoesNotExist:
            user_data[USER_DATA['field_name']] = None
    return user_data


def ps_login(request: HttpRequest):
    if request.method == 'POST':
        post = json.loads(request.body)
        user = authenticate(
            request, username=post['username'], password=post['password'])
        if user is not None:
            login(request, user)
            return JsonResponse({'user': get_user_data(request, user), 'authenticated': True})
        else:
            return JsonResponse({'authenticated': False})
    else:
        if request.user.is_authenticated:
            return JsonResponse({'user': get_user_data(request, request.user), 'authenticated': True})
        else:
            return JsonResponse({'authenticated': False})


@login_required
def ps_logout(request: HttpRequest):
    logout(request)
    return JsonResponse({'success': True})


def google_login(request: HttpRequest):
    if request.user.is_authenticated:
        return JsonResponse({'message': 'Already logged in', 'user': get_user_data(request, request.user), 'authenticated': True})
    token = json.loads(request.body)['id_token']
    try:
        idinfo = id_token.verify_oauth2_token(
            token, requests.Request(), settings.GOOGLE_CLIENT_ID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')
    except ValueError:
        # Invalid token
        return JsonResponse({'authenticated': False})

    # This will create the user if it does not exist
    user = authenticate(request, idinfo=idinfo)
    login(request, user)
    return JsonResponse({'user': get_user_data(request, user), 'authenticated': True})


@login_required
def get_account_info(request: HttpRequest):
    try:
        ps_user = GoogleUser.objects.get(user=request.user)
        google_linked = False if ps_user.google_id is None else True
    except GoogleUser.DoesNotExist:
        google_linked = False
    return JsonResponse({'password_set': request.user.has_usable_password() and request.user.password != '', 'google_linked': google_linked})


@login_required
def change_password(request: HttpRequest):
    data = json.loads(request.body)
    if (not request.user.has_usable_password() or request.user.password == '') or request.user.check_password(data['current_password']):
        if data['new_password_1'] == data['new_password_2']:
            request.user.set_password(data['new_password_1'])
            request.user.save()
            update_session_auth_hash(request, request.user)
            return JsonResponse({'success': True})
    return JsonResponse({'success': False})


@login_required
def change_email(request: HttpRequest):
    data = json.loads(request.body)
    request.user.email = data['new_email']
    request.user.save()
    return JsonResponse({'success': True})


@login_required
def change_username(request: HttpRequest):
    data = json.loads(request.body)
    request.user.username = data['new_username']
    try:
        request.user.save()
        return JsonResponse({"success": True})
    except IntegrityError:
        return JsonResponse({'success': False})


@login_required
def unlink_google(request: HttpRequest):
    ps_user = GoogleUser.objects.get(user=request.user)
    ps_user.google_id = None
    ps_user.save()
    return JsonResponse({'success': True})


@login_required
def delete_account(request: HttpRequest):
    data = json.loads(request.body)
    if request.user.username == data['username'] and not request.user.is_superuser:
        request.user.delete()
        logout(request)
        return JsonResponse({"success": True})


@user_passes_test(lambda u: u.is_superuser)
def login_as(request: HttpRequest, user_id):
    logout(request)
    user = authenticate(request, user_id=user_id)
    login(request, user)
    return redirect('home')


def check_user(request: HttpRequest):
    data = json.loads(request.body)
    try:
        user = User.objects.get(username=data['username'])
    except User.DoesNotExist:
        return JsonResponse({'result': ['unknown']})
    options = []
    if WebAuthnCred.objects.filter(user__user=user).exists():
        options.append('webauthn')
    if user.password != '' and user.has_usable_password():
        options.append('password')
    if GoogleUser.objects.filter(user=user).exists():
        options.append('google')
    return JsonResponse({'result': options})
