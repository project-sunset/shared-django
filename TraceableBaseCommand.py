
import uuid
from datetime import datetime

from django.core.management.base import BaseCommand


class TraceableBaseCommand(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._trace_id = str(uuid.uuid4())
    
    def log(self, message: str):
        timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        self.stdout.write(f'{timestamp} {self._trace_id} {message}')