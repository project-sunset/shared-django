from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.http import HttpRequest
from rest_framework.authentication import BaseAuthentication
from shared_django.accounts.models import ApiToken, GoogleUser
from django.utils import timezone


class SupportBackend:
    def get_user(self, user_id=None):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def authenticate(self, request, user_id=None):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class GoogleBackend:
    def get_user(self, user_id=None):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def authenticate(self, request, idinfo=None):
        try:
            user = User.objects.get(googleuser__google_id=idinfo['sub'])
            return user
        except User.DoesNotExist:
            try:
                user = User.objects.get(email=idinfo['email'])
                try:
                    ps_user = GoogleUser.objects.get(user=user)
                    ps_user.google_id = idinfo['sub']
                    ps_user.save()
                except GoogleUser.DoesNotExist:
                    GoogleUser.objects.create(user=user, google_id=idinfo['sub'])
                return user
            except User.DoesNotExist:
                user = User.objects.create(email=idinfo['email'], username=idinfo['name'])
                GoogleUser.objects.create(user=user, google_id=idinfo['sub'])
                return user


class TokenAuthentication(BaseAuthentication):
    def authenticate(self, request: HttpRequest):
        try:
            token = ApiToken.objects.get(token=request.headers['authorization'])
            if token.expiry < timezone.now():
                token.delete()
                return None
            return (token.user, None)
        except (ApiToken.DoesNotExist, ValidationError, KeyError):
            return None
