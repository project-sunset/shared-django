import json
from unittest import TestCase, mock

import shared_django.b2django.backblaze
from django.test import tag

CONFIG = {
    "BUCKET_ID": "bucket-id",
    "APPLICATION_KEY_ID": "app-key-id",
    "APPLICATION_KEY": "app-key"
}

class MockedSession():
    def get(*args, **kwargs):
        return mocked_requests(*args, **kwargs)
    def post(*args, **kwargs):
        return mocked_requests(*args, **kwargs)


def mocked_requests(mock_session, *args, **kwargs):
    class MockResponse:
        def __init__(self, text, status_code, headers):
            self.text = text
            self.content = text
            self.status_code = status_code
            self.headers = headers

        def json(self):
            return json.loads(self.text)

    if args[0] == "https://api.backblazeb2.com/b2api/v2/b2_authorize_account":
        return MockResponse(json.dumps({"apiUrl": "api-url", "authorizationToken": "auth-token"}), 200, {})
    elif args[0] == "api-url/some-file":
        return MockResponse("file-content", 200, {'Content-Type': 'application/octet-stream'})
    elif args[0] == 'api-url/b2api/v2/b2_list_file_names':
        return MockResponse(json.dumps({'files': ['file1'], 'nextFileName': None}), 200, {'Content-Type': 'application/json'})
    elif args[0] == 'api-url/b2api/v2/b2_get_upload_url':
        return MockResponse(json.dumps({'uploadUrl': 'upload-url', 'authorizationToken': 'upload-token'}), 200, {'Content-Type': 'application/json'})
    elif args[0] == 'upload-url':
        return MockResponse(json.dumps({"upload": "ok"}), 200, {'Content-Type': 'application/json'})
    else:
        raise Exception(f"{args[0]} is not mocked")

@tag('backblaze')
class BackblazeTest(TestCase):

    @mock.patch('shared_django.b2django.backblaze.Session', side_effect=MockedSession)
    def test_download(self, mock_obj):
        self.bb = shared_django.b2django.backblaze.Backblaze(CONFIG)
        response = self.bb.download_file('/some-file')
        self.assertEqual(response, "file-content")

    @mock.patch('shared_django.b2django.backblaze.Session', side_effect=MockedSession)
    def test_list_file_names(self, mock_obj):
        self.bb = shared_django.b2django.backblaze.Backblaze(CONFIG)
        response = self.bb.list_file_names(prefix="something")
        self.assertEqual(response, ['file1'])

    @mock.patch('shared_django.b2django.backblaze.Session', side_effect=MockedSession)
    def test_upload_file(self, mock_obj):
        self.bb = shared_django.b2django.backblaze.Backblaze(CONFIG)
        class MockFile:
            def read(self):
                return b'upload-file-content'
        response = self.bb.upload_file("uploaded-file", MockFile())
        self.assertEqual(response["upload"], "ok")
