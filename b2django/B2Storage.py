from django.core.files.storage import Storage
from django.utils.deconstruct import deconstructible
from django.conf import settings
from django.core.cache import cache

from .backblaze import Backblaze

class B2SettingsMissing(Exception):
    pass

@deconstructible
class B2Storage(Storage):
    def __init__(self):

        if not hasattr(settings, 'B2'):
            raise B2SettingsMissing("B2 settings are missing")

        self.b2_backblaze = Backblaze({
            "BUCKET_ID": settings.B2['BUCKET_ID'],
            "APPLICATION_KEY_ID": settings.B2['APPLICATION_KEY_ID'],
            "APPLICATION_KEY": settings.B2['APPLICATION_KEY']
        }, cache)

    def _open(self, name, mode='rb'):
        return self.b2_backblaze.download_file(name)

    def exists(self, name):
        return False

    def url(self, name):
        return f'https://{settings.B2["PUBLIC_URL"]}/file/{settings.B2["BUCKET_NAME"]}/{name}'

    def delete(self, name):
        file_id = self.b2_backblaze.list_file_names(name)[0]['fileId']
        self.b2_backblaze.delete_file_version(name, file_id)

    def _save(self, name, content):
        return self.b2_backblaze.upload_file(name, content)['fileName']
