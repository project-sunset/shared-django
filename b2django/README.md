# Django Backblaze B2

This Django app implements a Django Storage that stores files in Backblaze B2.

Example Django settings

```python
B2 = {
  # Bucket to use for Storage
  'BUCKET_ID': '',
  'BUCKET_NAME': '',
  'APPLICATION_KEY_ID': '',
  'APPLICATION_KEY': '',
  'PUBLIC_URL': '', # URL that points to bucket, for example: media.example.org
  # Bucket to use for DB backup
  'BACKUP_BUCKET_ID': '',
  'BACKUP_APPLICATION_KEY_ID': '',
  'BACKUP_APPLICATION_KEY': '',
}

from shared_django.b2django.B2Storage import B2Storage

B2_STORAGE = B2Storage()
```

Then in a model you can do:

```python
from django.conf import settings
from django.db import models

class Recording(models.Model):
    audio = models.FileField(storage=settings.B2_STORAGE)
```

For local development you can use in your settings:

```python
from django.core.files.storage import FileSystemStorage

B2_STORAGE = FileSystemStorage()
```
