import hashlib
import json
import logging
from requests import Session
from django.core.cache.backends.base import DEFAULT_TIMEOUT, BaseCache

LOGGER = logging.getLogger('backblaze')


class LocalCache(BaseCache):
    def __init__(self, *args, **kwargs):
        super().__init__({}, *args, **kwargs)
        self.data = {}

    def add(self, key, value, timeout=DEFAULT_TIMEOUT, version=None):
        self.data[key] = value
        return True

    def get(self, key, default=None, version=None):
        if key in self.data:
            return self.data[key]
        return None

    def set(self, key, value, timeout=DEFAULT_TIMEOUT, version=None):
        self.data[key] = value

    def touch(self, key, timeout=DEFAULT_TIMEOUT, version=None):
        return False

    def delete(self, key, version=None):
        del self.data[key]

    def has_key(self, key, version=None):
        return key in self.data

    def clear(self):
        self.data = {}


class Backblaze:
    def __init__(self, config, cache=LocalCache()):
        self.bucket_id = config["BUCKET_ID"]
        self.auth_url = "https://api.backblazeb2.com/b2api/v2/b2_authorize_account"
        self.application_key_id = config["APPLICATION_KEY_ID"]
        self.application_key = config["APPLICATION_KEY"]
        self.cache = cache
        self.api_version = '/b2api/v2/'
        self.session = Session()

    def list_file_names(self, prefix=""):
        next_file_name = None
        all_files = []
        i = 0
        while i < 10:
            i += 1
            response = self._fetch('post', 'b2_list_file_names', {
                'json': {
                    'bucketId': self.bucket_id,
                    'prefix': prefix,
                    'startFileName': next_file_name,
                }
            })
            all_files.extend(response['files'])
            if response['nextFileName'] == None:
                return all_files

    def upload_file(self, filename, file_object):
        file_data = file_object.read()
        sha1_of_file_data = hashlib.sha1(file_data).hexdigest()
        content_length = str(len(file_data))
        return self._fetch('post', 'upload_file', {
            'headers': {
                'X-Bz-File-Name': filename.replace('\\', '/'),
                'Content-Type': 'application/octet-stream',
                'Content-Length': content_length,
                'X-Bz-Content-Sha1': sha1_of_file_data
            },
            'data': file_data
        })

    def download_file(self, url):
        return self._fetch('get', url, {})

    def delete_file_version(self, file_name, file_id):
        return self._fetch('post', 'b2_delete_file_version', {
            'json': {
                'fileName': file_name,
                'fileId': file_id
            }
        })

    def _fetch(self, http_verb, url, requests_parameters, retry_count=0):
        fetch = self.session.post if http_verb == 'post' else self.session.get
        if 'headers' not in requests_parameters:
            requests_parameters['headers'] = {}
        if url == 'upload_file':
            auth = self._get_upload_url()
            requests_parameters['headers']['Authorization'] = auth['upload_token']
            final_url = auth['upload_url']
        else:
            auth = self._get_auth()
            if http_verb == 'post':
                requests_parameters['headers']['Authorization'] = auth['auth']
                final_url = '{}{}{}'.format(auth['api_url'], self.api_version, url)
            else:
                requests_parameters['params'] = {'Authorization': auth['auth']}
                final_url = '{}{}'.format(auth['api_url'], url)
        response = fetch(final_url, **requests_parameters)
        if response.status_code == 200:
            if response.headers['Content-Type'] == 'application/octet-stream':
                return response.content
            return response.json()
        elif response.status_code == 401:
            message = response.json()
            if message['code'] in ['bad_auth_token', 'expired_auth_token'] and retry_count == 0:
                if url == 'upload_file':
                    self.cache.delete('b2.uploadAuthToken')
                else:
                    self.cache.delete('b2.authorizationToken')
                return self._fetch(http_verb, url, requests_parameters, retry_count=retry_count+1)
            else:
                LOGGER.error('B2 %s: %s', url, response.text)
                raise Exception('B2 Error')
        elif (response.status_code == 500 or response.status_code == 503) and url == 'upload_file' and retry_count < 2:
            self.cache.delete('b2.uploadUrl')
            return self._fetch(http_verb, url, requests_parameters, retry_count=retry_count+1)
        else:
            LOGGER.error('B2 %s: %s', url, response.text)
            raise Exception('B2 Error')

    def _authorize_account(self):
        LOGGER.debug('Authorize to B2')
        response = self.session.get(self.auth_url, auth=(self.application_key_id, self.application_key))
        if response.status_code != 200:
            LOGGER.error('B2 Auth error: %s', response.text)
            raise Exception('B2 Error')
        data = json.loads(response.text)
        self.cache.set('b2.apiUrl', data['apiUrl'], 60*60*24)
        self.cache.set('b2.authorizationToken', data['authorizationToken'], 60*60*24)
        return {
            'auth': data['authorizationToken'],
            'api_url': data['apiUrl']
        }

    def _get_auth(self):
        api_url = self.cache.get('b2.apiUrl')
        auth = self.cache.get('b2.authorizationToken')
        if auth is None or api_url is None:
            return self._authorize_account()
        return {
            'auth': auth,
            'api_url': api_url
        }

    def _get_upload_url(self):
        LOGGER.debug('Get upload url for B2')
        upload_url = self.cache.get('b2.uploadUrl')
        upload_token = self.cache.get('b2.uploadAuthToken')
        if upload_url is None or upload_token is None:
            response = self._fetch('post', 'b2_get_upload_url', {
                'json': {
                    'bucketId': self.bucket_id
                }
            })
            self.cache.set('b2.uploadUrl', response['uploadUrl'], 60*60*24)
            self.cache.set('b2.uploadAuthToken', response['authorizationToken'], 60*60*24)
            return {
                'upload_url': response['uploadUrl'],
                'upload_token': response['authorizationToken']
            }
        return {
            'upload_url': upload_url,
            'upload_token': upload_token
        }
