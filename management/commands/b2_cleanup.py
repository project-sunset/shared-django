from django.apps import apps
from django.conf import settings
from django.core.management.base import CommandParser
from shared_django.b2django.backblaze import Backblaze
from shared_django.TraceableBaseCommand import TraceableBaseCommand


class Command(TraceableBaseCommand):
    help = "cleanup unused files from B2"

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument('app_name')
        parser.add_argument('model_name')
        parser.add_argument('field_name')
        parser.add_argument('--dry-run', action='store_true')

    def handle(self, *args, **kwargs):
        self.log("Cleaning up files...")
        self._cleanup(kwargs['app_name'], kwargs['model_name'], kwargs['field_name'], kwargs['dry_run'])

    def _cleanup(self, app_name, model_name, field_name, dry_run):
        model = apps.get_app_config(app_name).get_model(model_name)
        b2backblaze = Backblaze({
            "BUCKET_ID": settings.B2['BUCKET_ID'],
            "APPLICATION_KEY_ID": settings.B2['APPLICATION_KEY_ID'],
            "APPLICATION_KEY": settings.B2['APPLICATION_KEY']
        })
        all_files_in_database = set(model.objects.values_list(field_name, flat=True))
        all_files_in_b2 = b2backblaze.list_file_names()
        all_filenames_in_b2 = set(map(lambda file: file['fileName'], all_files_in_b2))
        filenames_to_be_deleted = all_filenames_in_b2 - all_files_in_database
        if dry_run:
            self.log(f'Dry run: files to delete: {filenames_to_be_deleted}')
        else:
            self.log(f'Files to delete: {filenames_to_be_deleted}')
            files_to_be_deleted = filter(lambda file: file['fileName'] in filenames_to_be_deleted, all_files_in_b2)
            for file in files_to_be_deleted:
                self.log(f'Deleting file: {file["fileName"]}')
                b2backblaze.delete_file_version(file['fileName'], file['fileId'])
        self.log("Done.")
