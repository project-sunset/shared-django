import os

from django.conf import settings
from shared_django.b2django.backblaze import Backblaze
from shared_django.TraceableBaseCommand import TraceableBaseCommand


class Command(TraceableBaseCommand):
    help = "Back up the database to B2"

    def handle(self, *args, **kwargs):
        self.log("Backing up the database...")
        content_length = backup()
        self.log(f"Database backed up, size: {content_length} bytes")


def backup():
    b2backblaze = Backblaze({
        "BUCKET_ID": settings.B2['BACKUP_BUCKET_ID'],
        "APPLICATION_KEY_ID": settings.B2['BACKUP_APPLICATION_KEY_ID'],
        "APPLICATION_KEY": settings.B2['BACKUP_APPLICATION_KEY']
    })
    env = os.environ.get('DJANGO_SETTINGS_MODULE', 'backend.settings.default')
    env = env[env.rfind('.')+1:]
    with open(settings.DATABASES['default']['NAME'], 'rb') as db_file:
        response = b2backblaze.upload_file(f'backup-{settings.BACKUP_NAME}-{env}.sqlite3', db_file)
    return response['contentLength']
