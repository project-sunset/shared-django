from django.conf import settings
from shared_django.b2django.backblaze import Backblaze
from shared_django.TraceableBaseCommand import TraceableBaseCommand


class Command(TraceableBaseCommand):
    help = "Restore the database from B2"

    def handle(self, *args, **kwargs):
        self.log("Restoring the database...")
        b2 = Backblaze({
            "BUCKET_ID": settings.B2['BACKUP_BUCKET_ID'],
            "APPLICATION_KEY_ID": settings.B2['BACKUP_APPLICATION_KEY_ID'],
            "APPLICATION_KEY": settings.B2['BACKUP_APPLICATION_KEY']
        })
        database = b2.download_file(f'/file/ProjectSunset-backups/backup-{settings.BACKUP_NAME}-production.sqlite3')
        with open(settings.DATABASES['default']['NAME'], 'wb') as f:
            f.write(database)
        self.log("Database restored")
